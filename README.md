# Software Architect diploma project

## Stage 1

1. [Бизнес-цели](business_goals.md)

2. [Функицональные требования](functional_requirements.md)

3. [Анализ стейкхолдеров](stakeholder_analysis.md)

4. [Концептуальная архитектуру](conceptual_architecture.md)

5. [Риски реализации](implementation_risks.md)

6. [План разработки и расширения системы](development_plan.md)

7. [Критические бизнес-сценарии](business_scenarios.md)

8. [Атрибуты качества](quality_attributes.md)

9. [Нефункциональных требования](non_functional_requirements.md)

10. [Архитектурные опции](architectural_options.md)

## Stage 2

11. [Architectural Decision Records](adr.md)

12. [Основные сценарии использования](main_user_scenarios.md)

13. [Базовая архитектура](base_achitecture.md)

14. [Основные представления](views/views.md)

## Stage 3

15. [Анализ рисков](risk_analysis.md)

16. [Стоимость владения системой](system_ownership_cost.md)