# Список ADR

### ADR-001: Выбор архитектурного стиля
**Дата**: `2023-07-13`

**Контекст:** Необходимо выбрать подходящий архитектурный стиль для обеспечения модульности и масштабируемости приложения.

**Решение:** Принято использовать микросервисную архитектуру, разделяя различные функциональные компоненты на отдельные сервисы.

**Статус:** `Принято`

**Последствия:**
- улучшена возможность независимого масштабирования компонентов
- потребуется дополнительное управление коммуникацией между сервисами.

### ADR-002: Реализация социальных групп
**Дата:** `2023-07-15`

**Контекст:** Необходимо создать механизм социальных групп, чтобы пользователи могли общаться и формировать сообщества по интересам.

**Решение:** Использовать графовую базу данных для хранения связей между пользователями и формирования социальных групп.

**Статус:** `Принято`

**Последствия:**
- улучшена возможность формирования сообществ
- потребуются SRE c опытом поддержки этой бд

### ADR-003: Интеграция сторонних устройств
**Дата:** `2023-07-17`

**Контекст:** Пользователи хотят подключать сторонние устройства (датчики сердцебиения и другие) для отслеживания тренировок.

**Решение:** Разработать API для интеграции сторонних устройств и считывания данных о состоянии организма.

**Статус:** `Принято`

**Последствия:**
- высокая точность мониторинга
- потребуется обеспечение безопасности передачи данных
- большой парк устройств

### ADR-004: Выбор технологии для реализации поиска и рекомендаций
**Дата:** `2023-08-05`

**Контекст:** Необходимо выбрать подходящую технологию для реализации механизма поиска и рекомендаций пользователю,
исходя из их интересов и тренировочной истории.

**Решение:** Рассматривались алгоритмы машинного обучения (нейронные сети).
Принято решение использовать нейронные сети для более точных и персонализированных рекомендаций.

**Статус:** `Не принято`

**Последствия:**
- нейронные сети требуют больших вычислительных ресурсов и сложностей в обучении модели